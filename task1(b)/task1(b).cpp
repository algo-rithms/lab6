﻿#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>

// Функція сортування Шелла
void shellSort(std::vector<float>& arr) {
    int n = arr.size();
    // Створення інкрементальної послідовності
    std::vector<int> increments;
    for (int s = 1; ; s++) {
        int inc = (pow(3, s) - 1) / 2;
        if (inc > n / 2) // Зупинка, коли інкремент стає занадто великим
            break;
        increments.push_back(inc);
    }

    // Починаємо сортування з найбільшого інкременту
    for (int k = increments.size() - 1; k >= 0; k--) {
        int gap = increments[k];
        // Сортування вставками з цим інкрементом
        for (int i = gap; i < n; i++) {
            float temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

int main() {
    // Генерація випадкових чисел
    srand(time(0));
    std::vector<float> arr;
    for (int i = 0; i < 20; i++) {
        float num = static_cast<float>(rand() % 201 + (rand() % 100) / 100.0);
        arr.push_back(num);
    }

    // Виведення вихідного масиву
    std::cout << "Original array: ";
    for (float x : arr) {
        std::cout << x << " ";
    }
    std::cout << "\n";

    // Сортування масиву
    shellSort(arr);

    // Виведення відсортованого масиву
    std::cout << "Sorted array:   ";
    for (float x : arr) {
        std::cout << x << " ";
    }
    std::cout << "\n";

    return 0;
}