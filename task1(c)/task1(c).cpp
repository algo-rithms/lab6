﻿#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

// Функція для сортування підрахунком
void countingSort(std::vector<char>& arr) {
    int range = 91; // Діапазон чисел (-100 до -10 включно)
    std::vector<int> count(range, 0);
    std::vector<char> output(arr.size());

    // Підрахунок кількості кожного елемента
    for (char num : arr) {
        count[num + 100]++; // Зміщення індексу, так що -100 стає 0
    }

    // Зміна count[i], щоб count[i] тепер містила позицію цього елемента в виводі
    for (int i = 1; i < range; i++) {
        count[i] += count[i - 1];
    }

    // Побудова вихідного масиву
    for (int i = arr.size() - 1; i >= 0; i--) {
        output[count[arr[i] + 100] - 1] = arr[i];
        count[arr[i] + 100]--;
    }

    // Копіювання вихідного масиву до arr, так що arr тепер містить відсортовані елементи
    for (int i = 0; i < arr.size(); i++) {
        arr[i] = output[i];
    }
}

int main() {
    srand(time(0));
    std::vector<char> arr;

    // Генерування випадкових значень типу char від -100 до -10
    for (int i = 0; i < 20; i++) {
        char num = static_cast<char>(rand() % 91 - 100); // Випадкові числа від -100 до -10
        arr.push_back(num);
    }

    // Виведення вихідного масиву
    std::cout << "Original array: ";
    for (char x : arr) {
        std::cout << static_cast<int>(x) << " ";
    }
    std::cout << "\n";

    // Сортування масиву
    countingSort(arr);

    // Виведення відсортованого масиву
    std::cout << "Sorted array:   ";
    for (char x : arr) {
        std::cout << static_cast<int>(x) << " ";
    }
    std::cout << "\n";

    return 0;
}
