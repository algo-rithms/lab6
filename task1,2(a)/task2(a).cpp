﻿#include <iostream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <chrono>

// Функція для перебудови купи
void heapify(std::vector<double>& arr, int n, int i) {
    int largest = i; // Ініціалізуємо найбільший елемент як корінь
    int left = 2 * i + 1; // лівий = 2*i + 1
    int right = 2 * i + 2; // правий = 2*i + 2

    // Якщо лівий дочірній елемент більший за корінь
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // Якщо правий дочірній елемент більший, ніж найбільший на даний момент
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // Якщо найбільший не є коренем
    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        // Рекурсивно перебудовуємо купу для затронутої піддерева
        heapify(arr, n, largest);
    }
}

// Головна функція для сортування масиву з використанням пірамідального сортування
void heapSort(std::vector<double>& arr) {
    int n = arr.size();

    // Побудова купи (перегрупувати масив)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за одним витягуємо елементи з купи
    for (int i = n - 1; i >= 0; i--) {
        // Переміщуємо поточний корінь в кінець
        std::swap(arr[0], arr[i]);

        // Викликаємо процедуру heapify на зменшеній купі
        heapify(arr, i, 0);
    }
}

int main() {
    // Генерація випадкових чисел
    srand(time(0));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів

    for (int i = 0; i < 7; i++) {

        std::vector<double> arr;
        for (int i = 0; i < sizes[i]; i++) {
            arr.push_back(rand() % 110 - 10 + (rand() % 100) / 100.0); // Випадкові числа від -10 до 99.99
        }

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        heapSort(arr); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << sizes[i] << " elements took: " << duration.count() << " milliseconds." << std::endl;
       
    }
    return 0;
}
