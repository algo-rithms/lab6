﻿#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <chrono>

// Функція сортування Шелла
void shellSort(std::vector<float>& arr) {
    int n = arr.size();
    // Створення інкрементальної послідовності
    std::vector<int> increments;
    for (int s = 1; ; s++) {
        int inc = (pow(3, s) - 1) / 2;
        if (inc > n / 2) // Зупинка, коли інкремент стає занадто великим
            break;
        increments.push_back(inc);
    }

    // Починаємо сортування з найбільшого інкременту
    for (int k = increments.size() - 1; k >= 0; k--) {
        int gap = increments[k];
        // Сортування вставками з цим інкрементом
        for (int i = gap; i < n; i++) {
            float temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                arr[j] = arr[j - gap];
            }
            arr[j] = temp;
        }
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів

    for (int i = 0; i < 7; i++) {

        std::vector<float> arr;
        for (int i = 0; i < 20; i++) {
            float num = static_cast<float>(rand() % 201 + (rand() % 100) / 100.0);
            arr.push_back(num);
        }

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        shellSort(arr); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << sizes[i] << " elements took: " << duration.count() << " milliseconds." << std::endl;

    }

    return 0;
}
